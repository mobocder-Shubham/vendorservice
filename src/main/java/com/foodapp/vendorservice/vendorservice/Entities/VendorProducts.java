package com.foodapp.vendorservice.vendorservice.Entities;

import javax.persistence.*;
import java.sql.Timestamp;

@Entity
public class VendorProducts {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String productName;
    private long productPriceInPaisa;
    private String productImage;
    private Timestamp createdAt;
    private Timestamp updatedAt;
    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    private VendorDetails vendor;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public long getProductPriceInPaisa() {
        return productPriceInPaisa;
    }

    public void setProductPriceInPaisa(long productPriceInPaisa) {
        this.productPriceInPaisa = productPriceInPaisa;
    }

    public String getProductImage() {
        return productImage;
    }

    public void setProductImage(String productImage) {
        this.productImage = productImage;
    }

    public Timestamp getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Timestamp createdAt) {
        this.createdAt = createdAt;
    }

    public Timestamp getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Timestamp updatedAt) {
        this.updatedAt = updatedAt;
    }

    public VendorDetails getVendor() {
        return vendor;
    }

    public void setVendor(VendorDetails vendor) {
        this.vendor = vendor;
    }
}
