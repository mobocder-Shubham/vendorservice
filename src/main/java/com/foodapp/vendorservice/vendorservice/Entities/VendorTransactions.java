package com.foodapp.vendorservice.vendorservice.Entities;

import com.foodapp.vendorservice.vendorservice.Utility.Transactionstatus;

import javax.persistence.*;
import java.sql.Timestamp;

@Entity
public class VendorTransactions {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    private VendorDetails vendor;
    private String transactionId;
    private Long transactionPrice;
    private String orderAddress;
    private Transactionstatus transactionstatus;
    private Timestamp createdAt;
    private Timestamp updatedAt;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public VendorDetails getVendor() {
        return vendor;
    }

    public void setVendor(VendorDetails vendor) {
        this.vendor = vendor;
    }

    public String getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(String transactionId) {
        this.transactionId = transactionId;
    }

    public Long getTransactionPrice() {
        return transactionPrice;
    }

    public void setTransactionPrice(Long transactionPrice) {
        this.transactionPrice = transactionPrice;
    }

    public String getOrderAddress() {
        return orderAddress;
    }

    public void setOrderAddress(String orderAddress) {
        this.orderAddress = orderAddress;
    }

    public Transactionstatus getTransactionstatus() {
        return transactionstatus;
    }

    public void setTransactionstatus(Transactionstatus transactionstatus) {
        this.transactionstatus = transactionstatus;
    }

    public Timestamp getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Timestamp createdAt) {
        this.createdAt = createdAt;
    }

    public Timestamp getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Timestamp updatedAt) {
        this.updatedAt = updatedAt;
    }
}
