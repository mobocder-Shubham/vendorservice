package com.foodapp.vendorservice.vendorservice.Entities;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.Set;

@Entity
public class VendorDetails {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String name;
    private String userName;
    private String password;
    private String address;
    private Timestamp createdAt;
    private Timestamp updatedAt;
    private String vendorImage;
    private Boolean isEnabled;
    private String role;

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public Boolean getEnabled() {
        return isEnabled;
    }

    public void setEnabled(Boolean enabled) {
        isEnabled = enabled;
    }

    @OneToMany(mappedBy = "vendor", fetch = FetchType.LAZY,
            cascade = CascadeType.ALL)
    private Set<VendorProducts> vendorProducts;
    @OneToMany(mappedBy = "vendor", fetch = FetchType.LAZY,
            cascade = CascadeType.ALL)
    private Set<VendorTransactions> vendorTransactions;

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Set<VendorTransactions> getVendorTransactions() {
        return vendorTransactions;
    }

    public void setVendorTransactions(Set<VendorTransactions> vendorTransactions) {
        this.vendorTransactions = vendorTransactions;
    }

    public Timestamp getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Timestamp createdAt) {
        this.createdAt = createdAt;
    }

    public Timestamp getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Timestamp updatedAt) {
        this.updatedAt = updatedAt;
    }

    public String getVendorImage() {
        return vendorImage;
    }

    public void setVendorImage(String vendorImage) {
        this.vendorImage = vendorImage;
    }

    public Set<VendorProducts> getVendorProducts() {
        return vendorProducts;
    }

    public void setVendorProducts(Set<VendorProducts> vendorProducts) {
        this.vendorProducts = vendorProducts;
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public void setId(Long id) {
        this.id = id;
    }


    public Long getId() {
        return id;
    }
}
