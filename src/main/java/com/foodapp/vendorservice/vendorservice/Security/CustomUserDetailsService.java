package com.foodapp.vendorservice.vendorservice.Security;

import java.util.ArrayList;
import java.util.List;


import com.foodapp.vendorservice.vendorservice.Entities.VendorDetails;
import com.foodapp.vendorservice.vendorservice.Service.VendorDetailsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;



@Service
public class CustomUserDetailsService implements UserDetailsService{
	@Autowired
	private VendorDetailsService vendorDetailsService;
	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		VendorDetails vendor=vendorDetailsService.getVendorByUserName(username);
		if(vendor != null && vendor.getEnabled()) {
			List<GrantedAuthority> roleList=new ArrayList<>();
			roleList.add(new SimpleGrantedAuthority(vendor.getRole()));
			return new org.springframework.security.core.userdetails.User(vendor.getUserName(), vendor.getPassword(),roleList);
		}
		else {
		        throw new UsernameNotFoundException("username not found");
		    }
		
	}


}
