package com.foodapp.vendorservice.vendorservice.Controllers;

import com.foodapp.vendorservice.vendorservice.Service.VendorDetailsService;
import com.foodapp.vendorservice.vendorservice.models.VendorCreationRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/authenticate")
public class AuthenticationController {
    @Autowired
    private VendorDetailsService vendorDetailsService;

    @PostMapping("/signUp")
    public ResponseEntity<?> createUser(@RequestBody VendorCreationRequest vendorCreationRequest)
    {
        return ResponseEntity.ok(vendorDetailsService.createVendor(vendorCreationRequest));

    }

}
