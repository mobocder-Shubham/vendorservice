package com.foodapp.vendorservice.vendorservice.Service;

import com.foodapp.vendorservice.vendorservice.Entities.VendorDetails;
import com.foodapp.vendorservice.vendorservice.models.VendorCreationRequest;
import org.springframework.stereotype.Service;

public interface VendorDetailsService {

    public VendorDetails getVendorByUserName(String username);
    public VendorDetails createVendor(VendorCreationRequest vendorCreationRequest);
}
