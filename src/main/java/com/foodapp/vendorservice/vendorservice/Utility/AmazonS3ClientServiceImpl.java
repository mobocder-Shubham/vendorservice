package com.foodapp.vendorservice.vendorservice.Utility;
import com.amazonaws.AmazonServiceException;
import com.amazonaws.auth.AWSCredentialsProvider;
import com.amazonaws.regions.Region;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import com.amazonaws.services.s3.model.CannedAccessControlList;
import com.amazonaws.services.s3.model.DeleteObjectRequest;
import com.amazonaws.services.s3.model.PutObjectRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Base64;
import java.util.UUID;


@Service
public class AmazonS3ClientServiceImpl {
    private String awsS3AudioBucket; //bucket name
    private AmazonS3 amazonS3; // s3 object which uploads file
   

    @Autowired
    public AmazonS3ClientServiceImpl(Region awsRegion, AWSCredentialsProvider awsCredentialsProvider, String awsS3AudioBucket) {
        this.amazonS3 = AmazonS3ClientBuilder.standard()
                .withCredentials(awsCredentialsProvider)
                .withRegion(awsRegion.getName()).build();
        this.awsS3AudioBucket = awsS3AudioBucket;
    }


    public String uploadFileToS3Bucket(String image, boolean enablePublicReadAccess) {
        String uploadedfile = ""; // the file path which is on s3
        byte[] fileData = Base64.getDecoder().decode(image);
         String fileName = UUID.randomUUID().toString().replace("-", "")+".png";

        try {
            //creating the file in the server (temporarily)
            File file = new File(fileName);
            FileOutputStream fos = new FileOutputStream(fileName);
            fos.write(fileData);
            fos.close();

            PutObjectRequest putObjectRequest = new PutObjectRequest(this.awsS3AudioBucket, fileName, file);

            if (enablePublicReadAccess) {
                putObjectRequest.withCannedAcl(CannedAccessControlList.PublicRead);
            }
            this.amazonS3.putObject(putObjectRequest);
            uploadedfile = String.valueOf(this.amazonS3.getUrl(awsS3AudioBucket, fileName));
            System.out.println(this.amazonS3.getUrl(awsS3AudioBucket, fileName));
            System.out.println(uploadedfile);


            //removing the file created in the server
            file.delete();
        } catch (IOException | AmazonServiceException ex) {
           System.out.println(ex.toString());
        }
        return uploadedfile;
    }

    public void deleteFileFromS3Bucket(String fileName) {
      
        final DeleteObjectRequest deleteObjectRequest = new DeleteObjectRequest(this.awsS3AudioBucket, fileName);
        amazonS3.deleteObject(deleteObjectRequest);
     
    }
}
