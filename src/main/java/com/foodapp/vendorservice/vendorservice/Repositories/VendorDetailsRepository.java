package com.foodapp.vendorservice.vendorservice.Repositories;

import com.foodapp.vendorservice.vendorservice.Entities.VendorDetails;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface VendorDetailsRepository extends PagingAndSortingRepository<VendorDetails,Long> {

}
