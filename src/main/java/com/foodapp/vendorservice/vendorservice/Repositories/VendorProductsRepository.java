package com.foodapp.vendorservice.vendorservice.Repositories;

import com.foodapp.vendorservice.vendorservice.Entities.VendorProducts;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface VendorProductsRepository extends PagingAndSortingRepository<VendorProducts,Long> {

}
