package com.foodapp.vendorservice.vendorservice.ServiceImpl;

import com.foodapp.vendorservice.vendorservice.Entities.VendorDetails;
import com.foodapp.vendorservice.vendorservice.Repositories.VendorDetailsRepository;
import com.foodapp.vendorservice.vendorservice.Service.VendorDetailsService;
import com.foodapp.vendorservice.vendorservice.models.VendorCreationRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

@Service
public class VendorDetailsServiceImpl implements VendorDetailsService {

    @Autowired
    private VendorDetailsRepository vendorDetailsRepository;
    @Autowired
    private BCryptPasswordEncoder bCryptPasswordEncoder;
    @Override
    public VendorDetails getVendorByUserName(String username) {
        return null;
    }

    @Override
    public VendorDetails createVendor(VendorCreationRequest vendorCreationRequest) {
        VendorDetails vendorDetails=new VendorDetails();
        vendorDetails.setUserName(vendorCreationRequest.getUsername());
        vendorDetails.setName(vendorCreationRequest.getName());
        vendorDetails.setPassword(bCryptPasswordEncoder.encode(vendorCreationRequest.getPassword()));
        return vendorDetailsRepository.save(vendorDetails);
    }
}
